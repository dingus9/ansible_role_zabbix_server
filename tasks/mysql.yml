---
- name: "set the correct delegated_dbhost"
  set_fact:
    delegated_dbhost: "{{ zabbix_server_dbhostname if (zabbix_server_dbhostname != 'localhost') else inventory_hostname }}"

- name: "Set the host ip address for the database user"
  set_fact:
    zabbix_database_host: "{{ hostvars[inventory_hostname]['ansible_' + zabbix_server_database_interface]['ipv4']['address'] }}"

# task file for mysql
- name: "mysql | create database user"
  mysql_user:
    name: "{{ zabbix_server_dbuser }}"
    password: "{{ zabbix_server_dbpassword }}"
    host: "{{ zabbix_database_host }}"
    login_user: "{{ zabbix_server_mysql_login_user }}"
    login_password: "{{ zabbix_server_mysql_login_password }}"
    priv: "{{ zabbix_server_dbname }}.*:ALL"
    state: present
  delegate_to: "{{ delegated_dbhost }}"
  run_once: True
  when:
    - zabbix_server_database_creation

- name: "mysql - check if database exists"
  mysql_query:
    query: SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{{ zabbix_server_dbname }}'
    login_user: "{{ zabbix_server_mysql_login_user }}"
    login_password: "{{ zabbix_server_mysql_login_password }}"
  register: mysql_check_db
  delegate_to: "{{ delegated_dbhost }}"
  run_once: True

- name: "mysql - Set db_exists fact"
  set_fact:
    db_exists: "{% if mysql_check_db.rowcount[0] > 0 %}True{% else %}False{% endif %}"

- block:
    - name: "mysql - create database"
      mysql_db:
        login_user: "{{ zabbix_server_mysql_login_user }}"
        login_password: "{{ zabbix_server_mysql_login_password }}"
        name: "{{ zabbix_server_dbname }}"
        encoding: "{{ zabbix_server_dbencoding }}"
        collation: "{{ zabbix_server_dbcollation }}"
        state: present
      when:
        - zabbix_server_database_creation
      delegate_to: "{{ delegated_dbhost }}"
      register: zabbix_database_created
      notify:
        - restart zabbix-server

    - name: "mysql - get the file for create.sql"
      stat:
        path: "{{ zabbix_server_datafiles_path }}/create.sql.gz"
      changed_when: False
      failed_when: not db_create_file.stat.exists
      when:
        - zabbix_server_database_sqlload
      register: db_create_file

    - name: "mysql - create database and import file"
      mysql_db:
        login_host: "{{ zabbix_server_mysql_login_host }}"
        login_user: "{{ zabbix_server_dbuser }}"
        login_password: "{{ zabbix_server_dbpassword }}"
        login_port: "{{ zabbix_server_dbport }}"
        login_unix_socket: "{{ zabbix_server_mysql_login_unix_socket | default(omit) }}"
        name: "{{ zabbix_server_dbname }}"
        encoding: "{{ zabbix_server_dbencoding }}"
        collation: "{{ zabbix_server_dbcollation }}"
        state: import
        target: "{{ db_create_file.stat.path }}"
      register: zabbix_db_create
      when:
        - db_create_file.stat.exists
        - zabbix_database_created
        - zabbix_server_database_sqlload
      notify:
        - restart zabbix-server
  run_once: true
  when: not db_exists
  tags:
    - zabbix-server
    - database
