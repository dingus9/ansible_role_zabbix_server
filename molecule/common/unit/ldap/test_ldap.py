import json
import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zabbix')


@pytest.fixture()
def zabbix_auth(host, user="Admin", password="zabbix"):
    encoded_body = json.dumps(
        {
            "jsonrpc": "2.0",
            "method": "user.login",
            "id": 1,
            "auth": None,
            "params": {
                "user": user,
                "password": password
            }
        }
    )

    response = host.run(
            f"curl -s http://localhost/api_jsonrpc.php "
            f"-H 'Content-Type: application/json-rpc' -X POST "
            f"--data {json.dumps(encoded_body)}"
            )
    return json.loads(response.stdout)['result']


@pytest.fixture()
def create_zabbix_user(host, zabbix_auth):
    encoded_body = json.dumps(
        {
            "jsonrpc": "2.0",
            "method": "user.create",
            "params": {
                "alias": "test_user",
                "passwd": "blank",
                "usrgrps": [
                    {
                        "usrgrpid": "11"
                    }
                ],
                "user_medias": [
                    {
                        "mediatypeid": "1",
                        "sendto": [
                            "support@company.com"
                        ],
                        "active": 0,
                        "severity": 63,
                        "period": "1-7,00:00-24:00"
                    }
                ]
            },
            "auth": zabbix_auth,
            "id": 1
        }
    )

    response = host.run(
            f"curl localhost/api_jsonrpc.php "
            f"-H 'Content-Type: application/json-rpc' -X POST "
            f"--data {json.dumps(encoded_body)}"
            )
    return json.loads(response.stdout)['result']['userids'][0]


@pytest.fixture()
def zabbix_user_setup(request, create_zabbix_user, host, zabbix_auth):
    user = create_zabbix_user

    def delete_zabbix_user():
        encoded_body = json.dumps(
            {
                "jsonrpc": "2.0",
                "method": "user.delete",
                "params": [
                    user
                ],
                "auth": zabbix_auth,
                "id": 1
            }
        )
        host.run(
                f"curl http://localhost/api_jsonrpc.php "
                f"-H 'Content-Type: application/json-rpc' -X POST "
                f"--data {json.dumps(encoded_body)} "
                )

    request.addfinalizer(delete_zabbix_user)
    return user


def test_zabbix_ldap_authentication(host, zabbix_user_setup, zabbix_auth):
    zabbix_user_setup
    zabbix_auth
    encoded_body = json.dumps(
        {
            "jsonrpc": "2.0",
            "method": "user.login",
            "id": 1,
            "params": {
                "user": "test_user",
                "password": "testing"
            }
        }
    )

    response = host.run(
            f"curl -s http://localhost/api_jsonrpc.php "
            f"-H 'Content-Type: application/json-rpc' -X POST "
            f"--data {json.dumps(encoded_body)}"
            )
    assert json.loads(response.stdout).get('result'), \
        "Could not autheticate to Zabbix API. Wrong username or password?"
