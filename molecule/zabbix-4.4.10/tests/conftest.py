import os
import pytest
import yaml


@pytest.fixture
def ansible_vars():
    pth = os.path.realpath(os.path.join(__file__, '..', '..', 'converge.yml'))
    with open(pth) as ymlfile:
        ansible_vars = yaml.safe_load(ymlfile)[1]['roles'][0]['vars']

    return ansible_vars


@pytest.fixture
def zabbix_version(ansible_vars):
    version = ansible_vars['zabbix_server_version']

    return version
